<?php
/**
 * Description of XmlSource
 *
 * @author david.yell
 */

/**
 * We are going to need to the Xml Utility class to deal with the feeds
 */
App::uses('Xml','Utility');

class FeedSource extends DataSource{
    
    /**
     * Protected schema variable so the datasource has a schema
     */
    protected $_schema = array(
        'feed' => array(
            'title' => array(
                'type'=>'string',
                'null'=>true,
                'key'=>'primary',
                'length'=>255
            ),
            'link' => array(
                'type'=>'string',
                'null'=>true,
                'key'=>'primary',
                'length'=>255
            ),
            'description' => array(
                'type'=>'string',
                'null'=>true,
                'key'=>'primary',
                'length'=>255
            ),
            'built' => array(
                'type'=>'datetime',
                'null'=>true,
                'key'=>'primary'
            ),
            'language' => array(
                'type'=>'string',
                'null'=>true,
                'key'=>'primary',
                'length'=>2
            )
        ),
        'items' => array(
            'title' => array(
                'type'=>'string',
                'null'=>true,
                'key'=>'primary',
                'length'=>255
            ),
            'link' => array(
                'type'=>'string',
                'null'=>true,
                'key'=>'primary',
                'length'=>255
            ),
            'guid' => array(
                'type'=>'string',
                'null'=>true,
                'key'=>'primary',
                'length'=>255
            ),
            'description' => array(
                'type'=>'string',
                'null'=>true,
                'key'=>'primary',
                'length'=>255
            ),
            'pubdate' => array(
                'type'=>'datetime',
                'null'=>true,
                'key'=>'primary'
            )
        )
    );
    
    /**
     * Doing the setup and loading in the feed url
     * @param type $config 
     */
    public function __construct($config){
        parent::__construct($config);
    }
    
    /*
     * This has to here, http://book.cakephp.org/2.0/en/models/datasources.html
     */
    public function listSources(){
        return array('feed');
    }
    
    /*
     * This has to be here, http://book.cakephp.org/2.0/en/models/datasources.html
     */
    public function describe($model){
        return $this->_schema['feed'];
    }
    
    /**
     * Read some items from the feed into an array in line with the conditions passed
     * @param type $model Contains all the information about the model
     * @param type $queryData Contains all the query information passed into the read from Controller::find()
     *              Must include a 'feed' dimension in the options which holds the feed url
     * @return type array() Returns an array of items
     */
    public function read($model, $queryData){
        if(!isset($queryData['feed'])){
            throw new Exception;
        }
        // Load the feed
        $xml = Xml::build($queryData['feed']);
        $this->feedarray = Xml::toArray($xml);
        
        // Do we have any options set?
        if(isset($queryData['conditions'])){
            // Process the options
            if(isset($queryData['conditions']['limit'])){
                $limit = $queryData['conditions']['limit'] - 1;
            }
        }
        
        foreach($this->feedarray['rss']['channel']['item'] as $k => $item){
            $results[] = $item;
            
            if(isset($limit) && $k == $limit){
                break;
            }
        }
        
        return $results;
    }
    
}

?>
